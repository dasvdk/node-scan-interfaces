"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const node_cidr_1 = require("node-cidr");
const node_threader_1 = require("node-threader");
const p = require("ping-lite");
class Pinger {
    constructor() {
    }
    _getInterfaces() {
        let options = [];
        let allInterfaces = require('os').networkInterfaces();
        for (let intfName in allInterfaces) {
            options.push(...allInterfaces[intfName]
                .filter((_if) => _if.family === 'IPv4' && !_if.internal && node_cidr_1.ip.toInt(node_cidr_1.cidr.netmask(_if.cidr)) >= node_cidr_1.ip.toInt("255.255.255.0")));
        }
        return options;
    }
    _getIps(_cidr, getMin = false) {
        let low_cidr = getMin ? node_cidr_1.cidr.min(_cidr) + "/" + node_cidr_1.cidr.mask(_cidr) : _cidr;
        return node_cidr_1.cidr.ips(low_cidr);
    }
    ping(ip) {
        return new Promise((resolve, reject) => {
            let args = process.platform === "win32"
                ? ['-n', '1', '-w', '2000', ip]
                : ['-c', '1', ip];
            let ping = new p(ip, { args });
            ping.send((err, rtt) => {
                if (err)
                    reject(err);
                else
                    resolve({ ip: ip, rtt: rtt });
            });
            setTimeout(_ => ping.stop(), 250);
        });
    }
    pingAll(cidr = null) {
        let ifs = cidr === null ? this._getInterfaces().map((_if) => this._getIps(_if.cidr)) : [this._getIps(cidr)];
        if (!ifs || ifs.length === 0)
            return Promise.reject("No interfaces" + (cidr === null ? "" : " with cidr " + cidr));
        let ips = [].concat(...ifs);
        let tasks = ips.map((ip) => () => this.ping(ip));
        return new node_threader_1.Threader(30).start(tasks);
    }
}
exports.Pinger = Pinger;
//# sourceMappingURL=scan-interfaces.js.map