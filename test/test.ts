import { Pinger } from '../scan-interfaces'

new Pinger().pingAll("172.20.30.100/28")
    .then(res => console.log("Finished: " + res.length))
    .catch(err => console.error(err))