import { spawn } from 'child_process' 
import { cidr, ip } from 'node-cidr'
import { Threader } from 'node-threader'
import * as p from 'ping-lite'
import { NetworkInterfaceInfo, networkInterfaces } from 'os';

export class Pinger {
    constructor() {

    }

    _getInterfaces() : NetworkInterfaceInfo[] {
        let options = []
        let allInterfaces = require('os').networkInterfaces()
        for (let intfName in allInterfaces) {
          options.push(
            ...allInterfaces[intfName]
            .filter((_if: NetworkInterfaceInfo) => _if.family === 'IPv4' && !_if.internal && ip.toInt(cidr.netmask(_if.cidr)) >= ip.toInt("255.255.255.0"))
          )
        }
        return options
      }
    
    _getIps(_cidr: string, getMin: boolean = false): string[]
    {
        let low_cidr = getMin ? cidr.min(_cidr) + "/" + cidr.mask(_cidr) : _cidr
        return cidr.ips(low_cidr)
    }
    
    ping(ip: string): Promise<IPingResult>
    {
        return new Promise<IPingResult>((resolve, reject) => {
            let args = process.platform === "win32"
                ? [ '-n', '1', '-w', '2000', ip ] 
                : [ '-c', '1', ip ]
            let ping = new p(ip, { args })
            ping.send((err: any, rtt: number) => {
                if (err) reject(err)
                else resolve({ ip: ip, rtt: rtt })
            })
            setTimeout(_ => ping.stop(), 250)
        })
    }
    
    pingAll(cidr: string | null = null) : Promise<IPingResult[]> {
        let ifs : string[][] = cidr === null ? this._getInterfaces().map((_if: NetworkInterfaceInfo) => this._getIps(_if.cidr)) : [ this._getIps(cidr) ]
        if (!ifs || ifs.length === 0) return Promise.reject("No interfaces" + (cidr === null ? "" : " with cidr " + cidr))
        let ips : string[] = [].concat(...ifs)
        let tasks : (() => Promise<IPingResult>)[]  = ips.map((ip : string) => () => this.ping(ip))
        return new Threader<IPingResult>(30).start(tasks)
    }
}

export interface IPingResult {
    rtt: number
    ip: string
}